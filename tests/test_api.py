import allure


class ApiRouts:
    LIST_USERS = 'api/users?page=2'
    SINGLE_USER = 'api/users/2'
    CREATE_USER = 'api/users'
    LIST_RESOURCE = 'api/unknown'


@allure.tag('Smoke')
@allure.feature('LIST USERS')
class TestClassGetUsersList:
    @allure.title('Check list user status code and response data')
    def test_get_list_users(self, api_url):
        response = api_url.get(ApiRouts.LIST_USERS)
        with allure.step(f"Status code should be 200"):
            assert response.status_code == 200
        with allure.step("Convert response to json"):
            data = response.json()
        for i in range(0, int(len(data['data']))):
            with allure.step(f"Element {i} has id and value is not Nul"):
                assert data['data'][i]['id'] != 0
            with allure.step(f"Element {i} has email and value is not None"):
                assert data['data'][i]['email'] is not None
            with allure.step(f"Element {i} has first name and value is not None"):
                assert data['data'][i]['first_name'] is not None
            with allure.step(f"Element {i} has last name and value is not None"):
                assert data['data'][i]['last_name'] is not None
            with allure.step(f"Element {i} has avatar and value is not None"):
                assert data['data'][i]['avatar'] is not None

    @allure.title('Get Users page 2')
    def test_get_list_users_page(self, api_url):
        params_page = {
            "page": 2
        }
        response = api_url.get(ApiRouts.LIST_USERS, params=params_page)
        with allure.step(f"Status code should be 200"):
            assert response.status_code == 200
        with allure.step("Convert response to json"):
            data = response.json()
        for i in range(0, int(len(data['data']))):
            with allure.step(f"Element {i} has id and value is not Nul"):
                assert data['data'][i]['id'] != 0
            with allure.step(f"Element {i} has email and value is not None"):
                assert data['data'][i]['email'] is not None
            with allure.step(f"Element {i} has first name and value is not None"):
                assert data['data'][i]['first_name'] is not None
            with allure.step(f"Element {i} has last name and value is not None"):
                assert data['data'][i]['last_name'] is not None
            with allure.step(f"Element {i} has avatar and value is not None"):
                assert data['data'][i]['avatar'] is not None

    # @allure.title('Get Users page below zero')
    # def test_get_list_users_page_below_zero(self, api_url):
    #     params_page = {
    #         "page": -1
    #     }
    #     response = api_url.get(ApiRouts.LIST_USERS, params=params_page)
    #     with allure.step(f"Status code should be 400, bad request"):
    #         assert response.status_code == 400






@allure.tag('Smoke')
@allure.feature('SINGLE USER')
class TestClassGetSingleUser:
    @allure.title('Check single user status code and response data')
    def test_get_single_user(self, api_url):
        response = api_url.get(ApiRouts.SINGLE_USER)
        with allure.step(f"Status code should be 200"):
            assert response.status_code == 200
        with allure.step("Convert response to json"):
            data = response.json()
        with allure.step(f"Element has id and value is not Nul"):
            assert data['data']['id'] is not None
            assert data['data']['id'] != 0
        with allure.step(f"Element has email and value is not None"):
            assert data['data']['email'] is not None
            assert "@" in data['data']['email']
            assert "." in data['data']['email']
            assert "reqres.in" in data['data']['email']
        with allure.step(f"Element has first name and value is not None"):
            assert data['data']['first_name'] is not None
            assert str(data['data']['first_name'])[0].isupper()
        with allure.step(f"Element has last name and value is not None"):
            assert data['data']['last_name'] is not None
            assert str(data['data']['last_name'])[0].isupper()
        with allure.step(f"Element has avatar and value is not None"):
            assert data['data']['avatar'] is not None


@allure.tag('Smoke')
@allure.feature('CREATE USER')
class TestClassCreateUser:
    @allure.title('Check created user status code and response data')
    def test_post_create_user(self, api_url, user_data_from_json):
        response = api_url.post(ApiRouts.CREATE_USER, data=user_data_from_json)
        with allure.step(f"Status code should be 201"):
            assert response.status_code == 201
        with allure.step("Convert response to json"):
            data = response.json()
        with allure.step(f"Created user has name and value is not None"):
            assert data['name'] is not None
            assert data['name'] == user_data_from_json['name']
        with allure.step(f"Created user has job and value is not None"):
            assert data['job'] is not None
            assert data['job'] == user_data_from_json['job']
        with allure.step(f"Created user has id and value is not None"):
            assert data['id'] is not None
            assert data['id'] != 0
        with allure.step(f"Created user has createdAt and value is not None"):
            assert data['createdAt'] is not None
            assert data['createdAt'] != 0


@allure.tag('Smoke')
@allure.feature('LIST USERS')
class TestClassGetListResource:
    @allure.title('Check list user status code and the presence of basic elements')
    def test_get_list_resource_the_basic_elements(self, api_url):
        response = api_url.get(ApiRouts.LIST_RESOURCE)
        with allure.step(f"Status code should be 200"):
            assert response.status_code == 200
        with allure.step("Convert response to json"):
            data = response.json()
        with allure.step(f"Element has page and value is not None"):
            assert data['page'] is not None
            assert data['page'] != 0
        with allure.step(f"Element has per_page and value is not None"):
            assert data['per_page'] is not None
            assert data['per_page'] != 0
        with allure.step(f"Element has per_page and value is not None"):
            assert data['total'] is not None
            assert data['total'] != 0
        with allure.step(f"Element has per_page and value is not None"):
            assert data['total_pages'] is not None
            assert data['total_pages'] != 0

    @allure.title('Check list user response data')
    def test_get_list_resource_the_basic_data(self, api_url):
        response = api_url.get(ApiRouts.LIST_RESOURCE)
        with allure.step("Convert response to json"):
            data = response.json()
        for i in range(0, int(len(data['data']))):
            with allure.step(f"Element {i} has id and value is not Nul"):
                assert data['data'][i]['id'] is not None
                assert data['data'][i]['id'] != 0
            with allure.step(f"Element {i} has name and value is not None"):
                assert data['data'][i]['name'] is not None
            with allure.step(f"Element {i} has year and value is not None"):
                assert data['data'][i]['year'] is not None
                assert int(data['data'][i]['year']) > 0
            with allure.step(f"Element {i} has color and value is not None"):
                assert data['data'][i]['color'] is not None
                assert '#' in data['data'][i]['color']
            with allure.step(f"Element {i} has pantone_value and value is not None"):
                assert data['data'][i]['pantone_value'] is not None
                assert '-' in data['data'][i]['pantone_value']
